package plugins.stef.library;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Java 3D library (1.6.0) for Icy
 * 
 * @author Stephane Dallongeville
 */
public class Java3DPlugin extends Plugin implements PluginLibrary
{
    //
}
